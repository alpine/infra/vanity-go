package server

import (
	"fmt"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	"gitlab.alpinelinux.org/alpine/infra/vanity-go/pkg/repository"
)

type Config struct {
	ApiVersion int            `mapstructure:"apiVersion,omitempty"`
	Proxies    []*ConfigProxy `mapstructure:"proxies,omitempty"`
}

type ConfigProxy struct {
	Namespace    string
	Entries      map[string]string
	Repositories []*repository.Repository
}

var config *Config

// LoadConfig loads the configuration from the config file
// It returns an error if the config file is not found or if the config is invalid
func LoadConfig() error {
	var err error
	if err = viper.Unmarshal(&config); err != nil {
		return fmt.Errorf("error unmarshalling config")
	}

	switch viper.GetInt("apiVersion") {
	case 1:
		err = loadV1Config(config)
	}

	if err != nil {
		return err
	}
	return nil
}

// GetConfig returns the configuration of the proxy
func GetConfig() *Config {
	return config
}

// RepositoriesForNamespace returns the repositories for the given namespace
// If the namespace is empty or invalid, it returns nil
func RepositoriesForNamespace(namespace, path string) []*repository.Repository {
	module := namespace + path
	for _, proxy := range GetConfig().Proxies {
		if strings.HasPrefix(module, proxy.Namespace) {
			var length int
			var mostSpecificRepository *repository.Repository

			for _, repo := range proxy.Repositories {
				repoModule := strings.Trim(fmt.Sprintf("%s/%s", repo.Namespace, strings.Trim(repo.EntryPoint, "-")), "/")
				if strings.HasPrefix(module, repoModule) {
					if len(repoModule) > length {
						length = len(repoModule)
						mostSpecificRepository = repo
					}
				}
			}
			if mostSpecificRepository != nil {
				return []*repository.Repository{mostSpecificRepository}
			}
		}
	}
	return nil
}

// loadV1Config loads the config from the v1 format
func loadV1Config(cfg *Config) error {
	log.Info().Msgf("Load config from version v%d", viper.GetInt("apiVersion"))
	defer log.Info().Msgf("Loaded %d proxies", len(cfg.Proxies))

	for _, proxy := range cfg.Proxies {
		for entrypoint, destination := range proxy.Entries {
			repo, err := repository.New(entrypoint, destination, proxy.Namespace)
			if err != nil {
				log.Error().Err(err).Str("entrypoint", entrypoint).Str("destination", destination).Msg("error creating repository")
				return err
			}

			proxy.Repositories = append(proxy.Repositories, repo)
			log.Debug().Str("proxy", proxy.Namespace).Msgf("Loaded repository %s", entrypoint)
		}

		log.Debug().Str("proxy", proxy.Namespace).Msgf("Loaded %d repositories", len(proxy.Repositories))
	}
	return nil
}
